package ru.startandroid.roomtodolist.Fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.startandroid.roomtodolist.Models.IssueModel;
import ru.startandroid.roomtodolist.Models.IssueViewModel;
import ru.startandroid.roomtodolist.R;
import ru.startandroid.roomtodolist.UIelements.IssueAdapterFooter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link IssueFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link IssueFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IssueFragment extends Fragment implements IssueAdapterFooter.ChangeListener {

    private IssueViewModel mIssueViewModel;
    private EditText mEditWordView;
    RecyclerView recyclerView;
    IssueAdapterFooter adapterFooter;
    CheckBox checkBox;
    public List<IssueModel> mvIssue = new ArrayList<>();
    IssueModel issue;
    Context context;
    RecyclerView.ItemAnimator itemAnimator;
    View v;
    private OnFragmentInteractionListener mListener;
    static final String TAG = "MyLogsFragment";

    public IssueFragment() {
        // Required empty public constructor
    }

    public static IssueFragment newInstance(String param1, String param2) {
        IssueFragment fragment = new IssueFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "fragment on create");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_issue, null);
        context = v.getContext();
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        checkBox = v.findViewById(R.id.checkBox);
        recyclerView = v.findViewById(R.id.recyclerview);
        mEditWordView = v.findViewById(R.id.edit_word);
        mEditWordView.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!TextUtils.isEmpty(mEditWordView.getText())) {
                        String issueText = mEditWordView.getText().toString();
                        issue = new IssueModel(issueText, false);
                        mIssueViewModel.insert(issue);
                        mEditWordView.setText("");

                        Log.d("MyLogs", "database " + mIssueViewModel.getAllIssue().toString());

                    } else {
                        Toast.makeText(
                                context,
                                R.string.empty_not_saved,
                                Toast.LENGTH_LONG).show();
                    }
                    return true;
                }
                return false;
            }

        });
        setAdapterFooter(mvIssue);
         itemAnimator = new DefaultItemAnimator();
        recyclerView.setItemAnimator(itemAnimator);

        FloatingActionButton fab = v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(mEditWordView.getText())) {
                    String issueText = mEditWordView.getText().toString();
                    issue = new IssueModel(issueText, false);
                    mIssueViewModel.insert(issue);
                    mEditWordView.setText("");
                    Log.d("MyLogs", "database " + mIssueViewModel.getAllIssue().toString());

                } else {
                    Toast.makeText(
                            context,
                            R.string.empty_not_saved,
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        mIssueViewModel = ViewModelProviders.of(this).get(IssueViewModel.class);
        mIssueViewModel.getAllIssue().observe(this, new Observer<List<IssueModel>>() {
            @Override
            public void onChanged(@Nullable final List<IssueModel> issues) {
                // Update the cached copy of the words in the adapter.
                adapterFooter.setIssues(issues);
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void setAdapterFooter(List<IssueModel> mData) {
        adapterFooter = new IssueAdapterFooter(context, mData);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(llm);
        adapterFooter.setChangeListener(this);
        recyclerView.setAdapter(adapterFooter);
    }

    @Override
    public void onChangeState(int position, boolean state) {
        //Log.d("MyLogsChangeState", "We need to do update! " + String.valueOf(adapter.getItem(position).getIssueName()) + "  " + position + adapter.getItem(position).getIssueState());
        Log.d("MyLogsChangeState", "We need to do update! " + String.valueOf(adapterFooter.getItem(position).getIssueName()) + "  " + position + adapterFooter.getItem(position).getIssueState());
        // Log.d("MyLogsChangeState", "on  " + adapter.getItem(position).getIssueName() + "  " + state + "  id issue" + adapter.getItem(position).getId_issue());
        Log.d("MyLogsChangeState", "on  " + adapterFooter.getItem(position).getIssueName() + "  " + state + "  id issue" + adapterFooter.getItem(position).getId_issue());
        IssueModel issueN = new IssueModel(adapterFooter.getItem(position).getIssueName(), state);
        issueN.setId_issue(adapterFooter.getItem(position).getId_issue());
        mIssueViewModel.update(issueN);
        Log.d("MyLogsChangeState", "quantity to update  " + mIssueViewModel.updateQuantity(mvIssue));
    }

    @Override
    public void DeleteCompleted() {
        mIssueViewModel.deleteCompleted(mIssueViewModel.getCompleted());

    }

    @Override
    public void ActiveIssues() {
        setAdapterFooter(mIssueViewModel.getActive());
    }

    @Override
    public void AllIssues() {
        setAdapterFooter(mIssueViewModel.getAllIssue().getValue());
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
