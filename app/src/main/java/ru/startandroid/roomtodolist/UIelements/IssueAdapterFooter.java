package ru.startandroid.roomtodolist.UIelements;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import ru.startandroid.roomtodolist.Models.IssueModel;
import ru.startandroid.roomtodolist.R;

import java.util.List;

public class IssueAdapterFooter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private ChangeListener ChangeListener;
    private final LayoutInflater mInflater;
    int adapterPosition;
    private List<IssueModel> mIssue; // Cached copy of words

    public IssueAdapterFooter(Context context, List<IssueModel> data) {
        mInflater = LayoutInflater.from(context);
        this.mIssue = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("MyLogsFooter", "view type " + viewType);
        if (viewType == TYPE_ITEM) {
            //Inflating recycle view item layout
            Log.d("MyLogsFooter", "Type Item in view holder");
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, parent, false);
            return new IssueViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            //Inflating footer view
            Log.d("MyLogsFooter", "Type Footer in view holder");
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer, parent, false);
            return new FooterViewHolder(itemView);
        } else return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof FooterViewHolder) {
            Log.d("MyLogsFooter", "Bind footer holder instance of view holder ");
            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            footerHolder.btnDeleteF.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChangeListener.DeleteCompleted();
                    notifyItemRemoved(adapterPosition);
                    Log.d("MyLogsFooter", " DELETE ");
                }
            });
            footerHolder.btnAllF.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChangeListener.AllIssues();
                    Log.d("MyLogsFooter", " ALL Issues ");
                }
            });
            footerHolder.btnActiveF.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChangeListener.ActiveIssues();
                    Log.d("MyLogsFooter", "Active Issues ");
                }
            });
        } else if (holder instanceof IssueViewHolder) {
            Log.d("MyLogsFooter", "Bind Issueview holder instance of view holder ");
            IssueViewHolder issueViewHolder = (IssueViewHolder) holder;
            if (mIssue != null) {
                IssueModel current = mIssue.get(position);
                issueViewHolder.issueItemView.setText(current.getIssueName());
                issueViewHolder.bind(position);
            } else {
                // Covers the case of data not being ready yet.
                issueViewHolder.issueItemView.setText("No Issues in TODO List");
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == mIssue.size()) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        Button btnDeleteF, btnAllF, btnActiveF;

        public FooterViewHolder(View view) {
            super(view);
            btnActiveF = view.findViewById(R.id.button_activeF);
            btnAllF = view.findViewById(R.id.button_allF);
            btnDeleteF = view.findViewById(R.id.button_deletecompletedF);
        }
    }

    public class IssueViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView issueItemView;
        CheckBox issueState;

        private IssueViewHolder(View itemView) {
            super(itemView);
            issueItemView = itemView.findViewById(R.id.textViewName);
            itemView.setOnClickListener(this);
            issueState = itemView.findViewById(R.id.checkBox);
            issueState.setOnClickListener(this);


        }

        public void bind(final int position) {
            if (mIssue.get(position).getIssueState()) {
                issueState.setChecked(true);
            } else {
                issueState.setChecked(false);
            }
        }


        @Override
        public void onClick(View view) {
            Log.d("MyLogsChange", "on click in adapter view holder ");
            adapterPosition = getAdapterPosition();
            if (mIssue.get(adapterPosition).getIssueState()) {
                issueState.setChecked(false);
                mIssue.get(adapterPosition).setIssueState(false);
            } else {
                issueState.setChecked(true);
                mIssue.get(adapterPosition).setIssueState(true);
            }
            ChangeListener.onChangeState(getAdapterPosition(), mIssue.get(getAdapterPosition()).getIssueState());
        }

    }

    public interface ChangeListener {
        void onChangeState(int position, boolean state);

        void DeleteCompleted();

        void ActiveIssues();

        void AllIssues();
    }

    public void setChangeListener(ChangeListener ChangeListener) {
        this.ChangeListener = ChangeListener;
    }

    public void setIssues(List<IssueModel> issues) {
        mIssue = issues;
        notifyDataSetChanged();
    }

    public IssueModel getItem(int position) {
        return mIssue.get(position);
    }

    // getItemCount() is called many times, and when it is first called,
    // mIssue has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mIssue != null) return mIssue.size() + 1;
        else return 0;
    }
}


