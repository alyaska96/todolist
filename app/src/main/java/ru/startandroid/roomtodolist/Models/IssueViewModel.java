package ru.startandroid.roomtodolist.Models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import ru.startandroid.roomtodolist.RoomElements.IssueRepository;

public class IssueViewModel extends AndroidViewModel {
    private IssueRepository mRepository;
    private LiveData<List<IssueModel>> mAllIssue;
    //   private  List<IssueModel> mActive;
    private List<IssueModel> mCompleted;

    public IssueViewModel(Application application) {
        super(application);
        mRepository = new IssueRepository(application);
        mAllIssue = mRepository.getAllIssue();
        //  mActive = mRepository.getActive();
        mCompleted = mRepository.getCompleted();

    }

    public LiveData<List<IssueModel>> getAllIssue() {
        return mAllIssue;
    }

    public void insert(IssueModel issue) {
        mRepository.insert(issue);
    }

    public void deleteAll(List<IssueModel> issueModel) {
        mRepository.delete(issueModel);
    }

    public void deleteCompleted(List<IssueModel> issueModel) {
        mRepository.deleteCompleted(issueModel);
    }

    public void update(IssueModel issue) {
        mRepository.update(issue);
    }

    public int updateQuantity(List<IssueModel> issueModel) {

        return mRepository.updateQuantity(issueModel);
    }

    public List<IssueModel> getActive() {
        return mRepository.getActive();
        //  return mActive;
    }

    public List<IssueModel> getCompleted() {
        return mCompleted;
    }
}
