package ru.startandroid.roomtodolist.Models;

import android.arch.persistence.room.ColumnInfo;

import android.arch.persistence.room.Entity;

import android.arch.persistence.room.PrimaryKey;

import android.support.annotation.NonNull;


@Entity(tableName = "issue_table")
public class IssueModel {
    @PrimaryKey(autoGenerate = true)
    // @PrimaryKey
    @ColumnInfo(name = "id_issue")
    int id_issue;
    @NonNull
    @ColumnInfo(name = "issue_name")
    String issueName;
    @ColumnInfo(name = "issue_state")
    Boolean issueState;

    public IssueModel(@NonNull String issueName, Boolean issueState) {
        this.issueName = issueName;
        this.issueState = issueState;
        // this.issueState = false;
    }

    public IssueModel(@NonNull String issueName) {
        this.issueName = issueName;
        issueState = false;
    }

    public IssueModel() {
    }


    public int getId_issue() {
        return id_issue;
    }

    public void setId_issue(int id_issue) {
        this.id_issue = id_issue;
    }

    public String getIssueName() {
        return issueName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }

    public Boolean getIssueState() {
        return issueState;
    }

    public void setIssueState(Boolean issueState) {
        this.issueState = issueState;
    }

    @Override
    public String toString() {

        return "Issue Model { " + id_issue + "issue name" + issueName + " "
                + ",done: " + issueState + " }";
    }
}
