package ru.startandroid.roomtodolist.RoomElements;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ru.startandroid.roomtodolist.Models.IssueModel;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface IssueDao {

    @Insert
    void insert(IssueModel issue);

    @Update(onConflict = REPLACE)
    void update(IssueModel issue);

    @Update
    int updateQuantity(List<IssueModel> issues);

    @Query("DELETE FROM issue_table")
    void deleteAll();

    @Query("DELETE FROM issue_table WHERE issue_state =:state")
    void deleteCompleted(boolean state);

    @Query("SELECT * from issue_table")
    LiveData<List<IssueModel>> getAllIssue();

    @Query("SELECT * from issue_table WHERE issue_state =:state")
    List<IssueModel> getActive(boolean state);

    @Query("SELECT * from issue_table WHERE issue_state =:state")
    List<IssueModel> getCompleted(boolean state);
}

