package ru.startandroid.roomtodolist.RoomElements;


import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.util.Log;

import java.util.List;

import ru.startandroid.roomtodolist.Models.IssueModel;

public class IssueRepository {
    private IssueDao mIssueDao;
    private LiveData<List<IssueModel>> mAllIssue;
    //private List<IssueModel> mActive;
    private List<IssueModel> mCompleted;
    IssueDataBase db;

    public IssueRepository(Application application) {
        db = IssueDataBase.getDatabase(application);
        mIssueDao = db.issueDao();
        mAllIssue = mIssueDao.getAllIssue();
        // mActive = mIssueDao.getActive(false);
        mCompleted = mIssueDao.getCompleted(true);


    }

    //????
    public LiveData<List<IssueModel>> getAllIssue() {
        Log.d("MyLogs", "mAllIssue in Repository " + mAllIssue);
        return mAllIssue;
    }

    public void insert(IssueModel issue) {
        mIssueDao.insert(issue);

    }

    public int updateQuantity(List<IssueModel> issueModelList) {
        int updatedquantity = mIssueDao.updateQuantity(issueModelList);
        return updatedquantity;
    }

    public void delete(List<IssueModel> issueModel) {
        mIssueDao.deleteAll();
    }

    public void deleteCompleted(List<IssueModel> issueModel) {
        mIssueDao.deleteCompleted(true);
    }

    public void update(IssueModel issue) {
        mIssueDao.update(issue);
    }

    public List<IssueModel> getActive() {
        return mIssueDao.getActive(false);
        //return mActive;
    }

    public List<IssueModel> getCompleted() {
        Log.d("MyLogsActive", "Completed Issue in Repository " + mCompleted.size());
        return mCompleted;
    }

}
