package ru.startandroid.roomtodolist.RoomElements;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import ru.startandroid.roomtodolist.Models.IssueModel;

@Database(entities = {IssueModel.class}, version = 1) //???
public abstract class IssueDataBase extends RoomDatabase {

    public abstract IssueDao issueDao();

    private static volatile IssueDataBase INSTANCE;

    static IssueDataBase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (IssueDataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            IssueDataBase.class, "issue_database")
                            .addCallback(sRoomDatabaseCallback).allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);

                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final IssueDao mDao;

        PopulateDbAsync(IssueDataBase db) {
            mDao = db.issueDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {

            Log.d("MyLogs", "instance " + INSTANCE.toString());
            // Log.d("MyLogs","instance query " + INSTANCE.mDatabase.query("issue_table").getColumnName(1));
            Log.d("MyLogs", "getAll Issues " + INSTANCE.issueDao().getAllIssue());
            Log.d("MyLogs", "issueDao to string  " + INSTANCE.issueDao().toString());

            return null;
        }
    }

}

