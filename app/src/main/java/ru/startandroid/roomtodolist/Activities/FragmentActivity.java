package ru.startandroid.roomtodolist.Activities;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import ru.startandroid.roomtodolist.Fragments.IssueFragment;
import ru.startandroid.roomtodolist.R;

public class FragmentActivity extends AppCompatActivity implements IssueFragment.OnFragmentInteractionListener {
    IssueFragment issueFragment;
    static final String TAG = "MyLogsFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        Log.d(TAG, "fragment Activity on create");
        issueFragment = new IssueFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frgmCont, issueFragment).addToBackStack(null).commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Log.d(TAG, "some listener");
    }
}
